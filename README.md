# Software-Engineering-Tutorials-Point-Notes
Software engineering is an engineering branch associated with development of software product using well-defined scientific principles, methods and procedures. The outcome of software engineering is an efficient and reliable software product.

## Software Product
 - Requirements
 - Software Analysis
 - System Design
 - Code Design
 - Testing
 - Deployment
 - Maintenance
 - Updates
 
Fritz Bauer, a German computer scientist, defines software engineering as:

  Software engineering is the establishment and use of sound engineering principles in order to obtain economically software that is reliable and work efficiently on real machines.


## Software Evolution
  - Change Request
  - Impact Analysis
  - Release Planning
  - System Update
  - System Release
  
## Software Evolution Laws

- S-type(static-type)
- P-type(practical-type)
- E-type(embedded-type)

## E-Type Software Evolution
  - Contijnuous Change
  - Increasing Complexity
  - Conservation of familiarity
  - Continuing Growth
  - Reducing Quality
  - Feedback Systems
  - Self-Regulation
  - Organized Stability
  
## Software Paradigms
   Programming-> Software-Design -> Software Development

## Software Design Paradigm
  - Design 
  - Maintenance
  - Programming

## Programming Paradigm
 - Coding
 - Testing 
 - Integration

## Characterstics of good software
  - operational
    - budget
    - usability
    - efficiency
    - correctness
    - Functionality
    - dependability
    - security
    - safety
  - transitional
    - portability
    - interoperability
    - reusability
    - adaptability
  - maintenance
    - Modularity
    - Maintainability
    - flexibility
    - scalability
  
## Software Development Life Cycle
  Software Development Life Cycle, SDLC for short, is a well-defined, structured sequence of stages in software engineering to develop the intended software product

## SDLC Activities
  - Communication
  - Request Gathering
  - Feasibility Study
  - System Analysis
  - Software Design
  - Coding
  - Testing
  - Integration
  - Implementation
  - Operation & Maintenance
  - Disposition

  ### Communication
  This is the first step where the user initiates the request for a desired software product. He contacts the service provider and tries to negotiate the terms. He submits his request to the service providing organization in writing.
  
  ### Requirement Gathering
  This step onwards the software development team works to carry on the project. The team holds discussions with various stakeholders from problem domain and tries to bring out as much information as possible on their requirements. The requirements are contemplated and segregated into user requirements, system requirements and functional requirements. The requirements are collected using a number of practices as given -
  
    - studying the existing or obsolete system and software,
    - conducting interviews of users and developers,
    - referring to the database or
    - collecting answers from the questionnaires.
    
  ### Feasibility Study
  It is found out, if the project is financially, practically and technologically feasible for the organization to take up.
  
  ### Software Design
  The output of this step comes in the form of two designs; logical design and physical design. Engineers produce meta-data and data dictionaries, logical diagrams, data-flow diagrams and in some cases pseudo codes.
  
  ### Coding
  writing program code in the suitable programming language and developing error-free executable programs efficiently.
  
  ### Testing
  An estimate says that 50% of whole software development process should be tested. Errors may ruin the software from critical level to its own removal. Software testing is done while coding by the developers and thorough testing is conducted by testing experts at various levels of code such as module testing, program testing, product testing, in-house testing and testing the product at user’s end. Early discovery of errors and their remedy is the key to reliable software.
  
  ### Integration
  Software may need to be integrated with the libraries, databases and other program(s). This stage of SDLC is involved in the integration of software with outer world entities.
  
  ### Implementation
  This means installing the software on user machines. At times, software needs post-installation configurations at user end. Software is tested for portability and adaptability and integration related issues are solved during implementation.
  
  ### Operation & Maintenance
  This phase confirms the software operation in terms of more efficiency and less errors. If required, the users are trained on, or aided with the documentation on how to operate the software and how to keep the software operational. The software is maintained timely by updating the code according to the changes taking place in user end environment or technology. This phase may face challenges from hidden bugs and real-world unidentified problems.
  
  ### Disposition
  This phase includes archiving data and required software components, closing down the system, planning disposition activity and terminating system at appropriate end-of-system time.



## Software Development Paradigm

   ### WaterFall Model
   This works if developers already worked with this domain or have previous knowledge.
 
   ### Iterative Model
   After each iteration, the management team can do work on risk management and prepare for the next iteration. Because a cycle includes small portion of whole software process, it is easier to manage the development process but it consumes more resources.
   
   ### Spiral Model
   This model considers risk, which often goes un-noticed by most other models. The model starts with determining objectives and constraints of the software at the start of one iteration. Next phase is of prototyping the software. This includes risk analysis. Then one standard SDLC model is used to build the software. In the fourth phase of the plan of next iteration is prepared.
   
   ### V-Model
    - Requirement Gathering
    - System Analysis
    - Software Design
    - Module Design
    - Coding
    - Unit Testing
    - Integration Testing
    - System Testing
    - Acceptance Testing
    
   ### BigBang Model
  

## Software Project Management
   ### Project Manager Responsibilities
   
   1. Managing People
     
     1. Act as A Project Leader
     2. Liaison with StakeHolders
     3. Managing Human Resources
     4. Setting Up reporting Hierarchy etc.
      
   2. Managing Project
     
     1. Defining and Settign up project scope
     2. Managing progress and performance
     3. Risk analysis at every phase
     4. take necessary step to avoid or come out of problems
     5. Act as project spokeperson
    
## Software Management Activities
  - Project Planning
  - Scope Management
  - Project Estimation
  
## Project Estimation

  - Sotware Size Estimation
  - Effort Estimation
  - Time Estimation 
 
 ### Software Size Estimation:
 Software Size may be estimated either iun terms of KLOC [Kilo Line of Code] or by calculating number of function points in the software. Lines Of code depends upon Software Practice and function points very according to the user requirements.

 ### Effort Estimation
 The managers estimate efforts in terms of personnel requirement and man-hour required to produce the software. For effort estimation software size should be known. This can either be derived by managers’ experience, organization’s historical data or software size can be converted into efforts by using some standard formulae.
 
 ### Time estimation
 The sum of time required to complete all tasks in hours or days is the total time invested to complete the project.
 
 ### Cost Estimation
  Size of software
  Software quality
  Hardware
  Additional software or tools, licenses etc.
  Skilled personnel with task-specific skills
  Travel involved
  Communication
  Training and support
  
 
## Project Estimation technique 
  
  1. Decomposition Techniques
   1. line Of code
   2. Function Points
  2. Empirical Estimation Technique
   1. Putnam Model Rayleigh Curve
   2. COCOCMO - COnstructive COst MOdel.  organic semi-detached and embedded
 
## Project Scheduling:
 
 For scheduling a project it is necessary to -

 1. Break down the project tasks into smaller, manageable form
 2. Find out various tasks and correlate them
 3. Estimate time frame required for each task
 4. Divide time into work-units
 5. Assign adequate number of work-units for each task
 6. Calculate total time required for the project from start to finish

## Resource Management:
 
 Resource Management includes - 
  
  1. Defining proper organization project by creating a project team and allocating responsibilities to each team member
  2. Determining resources required at a particular stage and their availability
  3. Manage Resources by generating resource request when they are required and de-allocating them when they are no more needed. 



